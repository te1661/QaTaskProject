import com.codeborne.selenide.testng.TextReport;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
@Listeners(TextReport.class)
public class XYZBankTest {
    @BeforeMethod
    public void setUp() {
        TextReport.onSucceededTest = false;
        TextReport.onFailedTest = true;
        open("https://www.globalsqa.com/angularJs-protractor/BankingProject/#/login");
    }
    @Test
    public void canLoginAsAnExistingUser(){
        $(By.xpath("//button[text()=\"Customer Login\"]")).click();
        String userName;
        $(By.name("userSelect")).selectOption(1);
        userName= $(By.name("userSelect")).getSelectedText();
        $(By.xpath("//button[text()=\"Login\"]")).click();
        $(By.className("fontBig")).shouldHave(text(userName));
    }

    @Test
    public void testDepositWithAnExistingAccount(){
        $(By.xpath("//button[text()=\"Customer Login\"]")).click();
        Integer userBalance;
        Integer deposit=20;
        $(By.name("userSelect")).selectOption(2);
        $(By.xpath("//button[text()=\"Login\"]")).click();
        userBalance = Integer.parseInt($(By.xpath("//div[text()[contains(.,\"Balance :\")]]/strong[2]")).getText());
        $(By.xpath("//button[@ng-click=\"deposit()\"]")).click();
        $(By.xpath("//input[@ng-model=\"amount\"]")).setValue(deposit.toString());
        $(By.xpath("//button[text()=\"Deposit\"]")).click();
        $(By.xpath("//span[text()=\"Deposit Successful\"]")).should(exist);
        $(By.xpath("//div[text()[contains(.,\"Balance :\")]]/strong[2]")).shouldHave(text(String.valueOf(userBalance+deposit)));
    }
    @Test
    public void testWithdrawlWithAnExistingAccount(){
        $(By.xpath("//button[text()=\"Customer Login\"]")).click();
        Integer userBalance;
        Integer withdrawl=10;
        $(By.name("userSelect")).selectOption(2);
        $(By.xpath("//button[text()=\"Login\"]")).click();
        userBalance = Integer.parseInt($(By.xpath("//div[text()[contains(.,\"Balance :\")]]/strong[2]")).getText());
        $(By.xpath("//button[@ng-click=\"withdrawl()\"]")).click();
        $(By.xpath("//input[@ng-model=\"amount\"]")).setValue(withdrawl.toString());
        $(By.xpath("//button[text()=\"Withdraw\"]")).click();
        $(By.xpath("//span[@ng-show=\"message\"]")).shouldHave(text("Transaction successful"));
        $(By.xpath("//div[text()[contains(.,\"Balance :\")]]/strong[2]")).shouldHave(text(String.valueOf(userBalance-withdrawl)));
    }
    @Test
    public void userTransactionListTest(){
        $(By.xpath("//button[text()=\"Customer Login\"]")).click();
        Integer deposit=200;
        Integer withdrawl=20;
        LocalDate today=LocalDate.now();
        $(By.name("userSelect")).selectOption(1);
        $(By.xpath("//button[text()=\"Login\"]")).click();
        $(By.xpath("//button[@ng-click=\"deposit()\"]")).click();
        $(By.xpath("//input[@ng-model=\"amount\"]")).setValue(deposit.toString());
        $(By.xpath("//button[text()=\"Deposit\"]")).click();
        $(By.xpath("//span[text()=\"Deposit Successful\"]")).should(exist);
        $(By.xpath("//button[@ng-click=\"withdrawl()\"]")).click();
        $(By.xpath("//form[@ng-submit=\"withdrawl()\"]/*/input[@ng-model=\"amount\"]")).setValue(withdrawl.toString());
        $(By.xpath("//button[text()=\"Withdraw\"]")).click();
        $(By.xpath("//span[text()=\"Transaction successful\"]")).should(exist);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        $(By.xpath("//button[@ng-click=\"transactions()\"]")).click();
        $(By.xpath("//input[@ng-model=\"startDate\"]")).sendKeys(today.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        $(By.xpath("//tr[@id=\"anchor0\"]/td[2]")).shouldHave(text(deposit.toString()));
        $(By.xpath("//tr[@id=\"anchor0\"]/td[3]")).shouldHave(text("Credit"));
        $(By.xpath("//tr[@id=\"anchor1\"]/td[2]")).shouldHave(text(withdrawl.toString()));
        $(By.xpath("//tr[@id=\"anchor1\"]/td[3]")).shouldHave(text("Debit"));
    }
}
