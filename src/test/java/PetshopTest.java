import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class PetshopTest {
    @Test
    public void getUnknownUserTest(){
        Response response=given().pathParam("username","user2").when().get("https://petstore.swagger.io/v2/user/{username}").then().extract().response();
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 404);
    }
    @Test
    public void postUserTest(){

        String body = "{\n" +
                "            \"id\": 0,\n" +
                "                \"username\": \"admin\",\n" +
                "                \"firstName\": \"firstAdmin\",\n" +
                "                \"lastName\": \"lastAdmin\",\n" +
                "                \"email\": \"admin@admin.admin\",\n" +
                "                \"password\": \"thebestadmin111\",\n" +
                "                \"phone\": \"123456789\",\n" +
                "                \"userStatus\": 0\n" +
                "        }";
        Response response=given().header("Content-Type", "application/json").and().body(body).when().post("https://petstore.swagger.io/v2/user").then().extract().response();
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);
        Response responseTwo=given().pathParam("username","admin").when().get("https://petstore.swagger.io/v2/user/{username}").then().extract().response();
        int statusCodeTwo = responseTwo.getStatusCode();
        Assert.assertEquals(statusCodeTwo, 200);
    }
    @Test
    public void deleteUserTest(){
        String body = "{\n" +
                "            \"id\": 1,\n" +
                "                \"username\": \"Nimda\",\n" +
                "                \"firstName\": \"firstNimda\",\n" +
                "                \"lastName\": \"lastNimda\",\n" +
                "                \"email\": \"nimda@nimda.nimda\",\n" +
                "                \"password\": \"thebadNimda111\",\n" +
                "                \"phone\": \"987654321\",\n" +
                "                \"userStatus\": 0\n" +
                "        }";
        Response response=given().header("Content-Type", "application/json").and().body(body).when().post("https://petstore.swagger.io/v2/user").then().extract().response();
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);
        Response responseTwo=given().pathParam("username","Nimda").when().delete("https://petstore.swagger.io/v2/user/{username}").then().extract().response();
        int statusCodeTwo = responseTwo.getStatusCode();
        Assert.assertEquals(statusCodeTwo, 200);

    }
}
